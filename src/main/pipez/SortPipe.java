package pipez;

import pipez.core.SimpleBlock;
import pipez.core.Block;
import pipez.core.Pipe;
import java.util.Arrays;


public class SortPipe implements Pipe  {
	
	
	private boolean ascending = false, descending = false;
	
	
	public String getName() {
		return "SortPipe";
	}
	
	
	
	private SortPipe(boolean ascending, boolean descending) {

		this.ascending = ascending;
		this.descending = descending;
	}
	
	public static SortPipe createAscending() {
		return new SortPipe(true, false);
	}
	
	public static SortPipe createDescending() {
		return new SortPipe(false, true);
	}
	
	
	
	@Override
	public Block transform(Block block) {
		SimpleBlock nb = new SimpleBlock();
		String[] fields = block.fields();
		
		if (ascending){
		for(int q=0; q<fields.length; q++) {
			String[] x = block.values();
		            int j;
		            boolean flag = true;  // will determine when the sort is finished
		            String temp;
		            while ( flag )
		            {
		                  flag = false;
		                  for ( j = 0;  j < x.length - 1;  j++ )
		                  {
		                          if ( x [ j ].compareToIgnoreCase( x [ j+1 ] ) > 0 )
		                          {                                             // ascending sort
		                                      temp = x [ j ];
		                                      x [ j ] = x [ j+1];     // swapping
		                                      x [ j+1] = temp; 
		                                      flag = true;
		                           } 
		                   } 
		            }
		            
			nb.add(x[q]);
		}
		}
		if(descending){
		for(int q=0; q<fields.length; q++) {
			String[] x = block.values();
		
		String t;
	    for(int i=0; i<x.length; i++) {
	        for(int j=0; j<x.length-1-i; j++) {
	        if(x[j+1].compareTo(x[j])>0) {
	            t= x[j];
	            x[j] = x[j+1];
	            x[j+1] = t;
	        }
	    }
	    }
	    nb.add(x[q]);
		}
		}
		
		return nb;
	}

	

}
