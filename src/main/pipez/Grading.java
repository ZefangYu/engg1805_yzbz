package pipez;
import pipez.core.SimpleBlock;
import pipez.core.Block;
import pipez.core.Pipe;
//This class takes in a block of two columns 
//First column contains names of student
//Second column contains marks of student
//This pipe returns a block of three columns
//Third column indicates the grades of each student
public class Grading implements Pipe{
	@Override
	public String getName() {
		return "Grading";
	}
	
	public static Grading create() {
		return new Grading();
	}

	@Override
	public Block transform(Block block) {
		SimpleBlock nb = new SimpleBlock();
		
			String[] x = block.values();
			String[] y = new String[x.length+1];
			System.arraycopy(x, 0, y, 0, x.length);
			if (Integer.parseInt(y[1])>=85){
				y[x.length]="HD";}
			else if (Integer.parseInt(y[1])>=75){
				y[x.length]="DI";}
			else if (Integer.parseInt(y[1])>=65){
				y[x.length]="CR";}
			else if (Integer.parseInt(y[1])>=50){
				y[x.length]="PASS";}
			else {
				y[x.length]="FAIL";}
			
			nb.add(y);
		
		return nb;
	}

}