package pipez;
import pipez.core.SimpleBlock;

import java.util.Random;
import pipez.core.Block;
import pipez.core.Pipe;

//This Pipe takes in a block containing one column nd adds a unique id to each name

public class SID implements Pipe {

	@Override
	public String getName() {
		return "SID";
	}
	
	private SID() {}
	
	public static SID create() {
		return new SID();
	}

	@Override
	public Block transform(Block block) {
		SimpleBlock nb = new SimpleBlock();
		for(String f :block.fields()) {
		
			//Creating a 13 digit unique ID
			Random random = new Random();
		    char[] digits = new char[13];
		    digits[0] = (char) (random.nextInt(9) + '1');
		    for (int i = 1; i < 13; i++) {
		        digits[i] = (char) (random.nextInt(10) + '0');
		    }
		    String b = new String(digits);
		    
			String[] x = block.values();
			String[] y = new String[x.length+1];
			System.arraycopy(x, 0, y, 0, x.length);
			y[x.length]=b;
			nb.add(y);
			}
		return nb;
	}

}
