package pipez;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
public class SortPipeTest {
	
	@Test
	//test empty block ascending method
	public void test_zero_columns1() throws Exception {
	
		SortPipe sp = SortPipe.createAscending();
		SimpleBlock sb = new SimpleBlock();
		Block tb = sp.transform(sb);
		assertThat(tb.fields().length, is(0));
		
	}
	
	@Test
	//test empty block Descending method
	public void test_zero_columns2() throws Exception {
	
		SortPipe sp = SortPipe.createDescending();
		SimpleBlock sb = new SimpleBlock();
		Block tb = sp.transform(sb);
		assertThat(tb.fields().length, is(0));
		
	}
	
	@Test
	public void test_Ascending_Digits() throws Exception {
	
		SortPipe sp = SortPipe.createAscending();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "5"); //first col1umn
		sb.add("C2", "2"); //second column
		sb.add("C3", "9"); //third column
		
		
		Block tb = sp.transform(sb);
		assertThat(tb.fields().length, is(3));
		assertThat(tb.value("C1"), is("2"));
		assertThat(tb.value("C2"), is("5"));
		assertThat(tb.value("C3"), is("9"));
	}
	
	@Test
	public void test_Descending_Digits() throws Exception {
	
		SortPipe sp = SortPipe.createDescending();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "5"); //first col1umn
		sb.add("C2", "2"); //second column
		sb.add("C3", "9"); //third column
		
		
		Block tb = sp.transform(sb);
		assertThat(tb.fields().length, is(3));
		assertThat(tb.value("C1"), is("9"));
		assertThat(tb.value("C2"), is("5"));
		assertThat(tb.value("C3"), is("2"));
	}
	
	@Test
	public void test_Descending_Alphabets() throws Exception {
	
		SortPipe sp = SortPipe.createDescending();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "a"); //first col1umn
		sb.add("C2", "b"); //second column
		sb.add("C3", "c"); //third column
		
		
		Block tb = sp.transform(sb);
		assertThat(tb.fields().length, is(3));
		assertThat(tb.value("C1"), is("c"));
		assertThat(tb.value("C2"), is("b"));
		assertThat(tb.value("C3"), is("a"));
	}
	
	@Test
	public void test_Ascending_Alphabets() throws Exception {
	
		SortPipe sp = SortPipe.createAscending();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "c"); //first col1umn
		sb.add("C2", "b"); //second column
		sb.add("C3", "a"); //third column
		
		
		Block tb = sp.transform(sb);
		assertThat(tb.fields().length, is(3));
		assertThat(tb.value("C1"), is("a"));
		assertThat(tb.value("C2"), is("b"));
		assertThat(tb.value("C3"), is("c"));
	}

}
