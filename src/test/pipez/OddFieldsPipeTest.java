package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;

public class OddFieldsPipeTest {

	@Test
	public void test_three_columns() throws Exception {
	
		OddFieldsPipe evp = OddFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); //first column
		sb.add("C2", "def"); //second column
		sb.add("C3", "GHI"); //third column
		
		
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(2));
		assertThat(tb.fields()[0], is("C1"));
		assertThat(tb.fields()[1], is("C3"));
	}
	
	@Test
	public void test_empty_block() {
		OddFieldsPipe evp = OddFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(0));
	}
	
	@Test
	public void test_one_column() throws Exception {
	
		OddFieldsPipe evp = OddFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); 
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(1));		
		assertThat(tb.fields()[0], is("C1"));
	}
	
	@Test
	public void test_two_columns() {
		OddFieldsPipe evp = OddFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); 
		sb.add("C2", "def");
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(1));
		assertThat(tb.fields()[0], is("C1"));
	}
	
	@Test
	public void test_four_columns() {
		OddFieldsPipe evp = OddFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); 
		sb.add("C2", "def");
		sb.add("C3", "GHI");
		sb.add("C4", "jkl");
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(2));
		assertThat(tb.fields()[0], is("C1"));
		assertThat(tb.fields()[1], is("C3"));
	}
	
	@Test
	public void test_five_columns() {
		OddFieldsPipe evp = OddFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); 
		sb.add("C2", "def");
		sb.add("C3", "GHI");
		sb.add("C4", "jkl");
		sb.add("C5", "MNO");
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(3));
		assertThat(tb.fields()[0], is("C1"));
		assertThat(tb.fields()[1], is("C3"));
		assertThat(tb.fields()[2], is("C5"));
	}
}
