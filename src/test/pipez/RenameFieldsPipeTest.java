package pipez;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static pipez.util.TestUtils.*;

public class RenameFieldsPipeTest {
	
	@Test
	public void test_col_rename() {
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "1");
		sb.add("C2", "2");
		sb.add("C3", "3");
		
		RenameFieldsPipe rn = RenameFieldsPipe.create(
								new String[]{"C1","C2","C3"}, 
								new String[]{"col1", "col2", "col3"} );
		Block b = rn.transform(sb);
		assertThat(b.fields(), are("col1","col2","col3"));
		assertThat(b.value("col1"), is("1"));
		assertThat(b.value("col2"), is("2"));
		assertThat(b.value("col3"), is("3"));
	}

	@Test
	public void test_some1_col_rename() {
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "1");
		sb.add("C2", "2");
		sb.add("C3", "3");
		
		RenameFieldsPipe rn = RenameFieldsPipe.create(
								new String[]{"C1"}, 
								new String[]{"col1"} );
		Block b = rn.transform(sb);
		assertThat(b.fields(), are("col1","C2","C3"));
		assertThat(b.value("col1"), is("1"));
		assertThat(b.value("C2"), is("2"));
		assertThat(b.value("C3"), is("3"));
	}
	
	@Test
	public void test_no_col_rename() {
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "1");
		sb.add("C2", "2");
		sb.add("C3", "3");
		
		RenameFieldsPipe rn = RenameFieldsPipe.create(
								new String[]{}, 
								new String[]{} );
		Block b = rn.transform(sb);
		assertThat(b.fields(), are("C1","C2","C3"));
		assertThat(b.value("C1"), is("1"));
		assertThat(b.value("C2"), is("2"));
		assertThat(b.value("C3"), is("3"));
	}
	@Test
	public void test_noname_col_rename() {
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "1");
		sb.add("C2", "2");
		sb.add("C3", "3");
		
		RenameFieldsPipe rn = RenameFieldsPipe.create(
								new String[]{"C1","C2","C3"}, 
								new String[]{"", "col2", "col3"} );
		Block b = rn.transform(sb);
		assertThat(b.fields(), are("","col2","col3"));
		assertThat(b.value(""), is("1"));
		assertThat(b.value("col2"), is("2"));
		assertThat(b.value("col3"), is("3"));
	}
	
	@Test
	public void test_many_col_rename() {
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "1");
		sb.add("C2", "2");
		sb.add("C3", "3");
		sb.add("C4", "4");
		sb.add("C5", "5");
		sb.add("C6", "6");
		
		RenameFieldsPipe rn = RenameFieldsPipe.create(
								new String[]{"C1","C2","C3","C4","C5","C6"}, 
								new String[]{"col1", "col2", "col3", "col4","col5","col6"} );
		Block b = rn.transform(sb);
		assertThat(b.fields(), are("col1","col2","col3","col4","col5","col6"));
		assertThat(b.value("col1"), is("1"));
		assertThat(b.value("col2"), is("2"));
		assertThat(b.value("col3"), is("3"));
		assertThat(b.value("col4"), is("4"));
		assertThat(b.value("col5"), is("5"));
		assertThat(b.value("col6"), is("6"));
	}

	@Test
	public void test_some2_col_rename() {
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "1");
		sb.add("C2", "2");
		sb.add("C3", "3");
		
		RenameFieldsPipe rn = RenameFieldsPipe.create(
								new String[]{"C2"}, 
								new String[]{"col2"} );
		Block b = rn.transform(sb);
		assertThat(b.fields(), are("C1","col2","C3"));
		assertThat(b.value("C1"), is("1"));
		assertThat(b.value("col2"), is("2"));
		assertThat(b.value("C3"), is("3"));
	}
	
}
