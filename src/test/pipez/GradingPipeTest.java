package pipez;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;

public class GradingPipeTest {
	
	
	@Test
	public void test_pass() {
		
		Grading sp = Grading.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "Zain"); //first col1umn
		sb.add("C2", "50"); //first col1umn
		Block tb = sp.transform(sb);
		assertThat(tb.value("C3"), is("PASS"));
		
	}
	@Test
	public void test_HD() {
		
		Grading sp = Grading.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "Zain"); //first col1umn
		sb.add("C2", "85"); //first col1umn
		Block tb = sp.transform(sb);
		assertThat(tb.value("C3"), is("HD"));
		
	}
	@Test
	public void test_DI() {
		
		Grading sp = Grading.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "Zain"); //first col1umn
		sb.add("C2", "75"); //first col1umn
		Block tb = sp.transform(sb);
		assertThat(tb.value("C3"), is("DI"));
		
	}
	@Test
	public void test_CR() {
		
		Grading sp = Grading.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "Zain"); //first col1umn
		sb.add("C2", "65"); //first col1umn
		Block tb = sp.transform(sb);
		assertThat(tb.value("C3"), is("CR"));
		
	}
	@Test
	public void test_FAIL() {
		
		Grading sp = Grading.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "Zain"); //first col1umn
		sb.add("C2", "49"); //first col1umn
		Block tb = sp.transform(sb);
		assertThat(tb.value("C3"), is("FAIL"));
		
	}
}
