package pipez;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;

public class SIDPipeTest {
	
	@Test
	public void empty_cloumn() {
		
		SID sp = SID.create();
		SimpleBlock sb = new SimpleBlock();
		Block tb = sp.transform(sb);
		assertThat(tb.fields().length, is(0));
		
	}
	
	@Test
	public void test_length_of_ID() {
		
		SID sp = SID.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "Zain"); //first col1umn
		Block tb = sp.transform(sb);
		assertThat(tb.value("C2").length(), is(13));
		
	}

}
