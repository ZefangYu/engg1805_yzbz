package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import static pipez.util.TestUtils.*;

public class FieldMatchPipeTest {

	@Test
	public void test_match() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		//
		FieldMatchPipe select = FieldMatchPipe.create("c3");//找到返回所有值
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi" ));
		
	}
	
	@Test
	public void test_nomatch() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.create("C5");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}
	
	@Test public void test_nomatch_case() {
		
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.create("C3");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}
	
	@Test
	public void test_match_inverse() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.createInverse("c5");//没有c5
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi" ));
	}

	@Test
	public void test_nomatch_inverse() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.createInverse("c3");//没找到返回所有值
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}

	@Test
	public void test_match_ignorecase() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.createIgnoreCase("C3");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi" ));
	}
	
	@Test
	public void test_nomatch_ignorecase() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.createIgnoreCase("C6");//忽略大小写
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}
	@Test
	public void test_nomatch_createInverseIgnoreCase() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.createInverseIgnoreCase("C3");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}
	@Test
	public void test_match_createInverseIgnoreCase() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		FieldMatchPipe select = FieldMatchPipe.createInverseIgnoreCase("C6");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(4));
	}
	
	@Test
	public void test_empty_block() {
		SimpleBlock sb = new SimpleBlock();
		
		FieldMatchPipe select = FieldMatchPipe.create("c2");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}
	
	@Test
	public void test_nofound_match() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		//
		FieldMatchPipe select = FieldMatchPipe.create("c8");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
		
	}
	
	@Test
	public void test_inversenofound_inverse() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi","123" );
		
		FieldMatchPipe select = FieldMatchPipe.createInverse("c8");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(5));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi","123" ));
	}
	
	@Test
	public void test_noblock_inverse() {
		SimpleBlock sb = new SimpleBlock();
		
		FieldMatchPipe select = FieldMatchPipe.create("c3");
		Block b = select.transform(sb);

		assertThat(b.values().length, is(0));
	}
}
