package pipez;

import static org.hamcrest.CoreMatchers.is; //IGNORE - 	eclipse mistakenly flags this as deprecated
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;
import pipez.core.SpecialBlocks;

public class IdentityPipeTest {

	@Test
	public void test_empty_block() {
		IdentityPipe id = IdentityPipe.create();
		Block b = id.transform(SpecialBlocks.EMPTY_BLOCK);
		assertThat(b, is(SpecialBlocks.EMPTY_BLOCK));
	}
	
	@Test
	public void test_single_field() throws Exception {
		IdentityPipe id = IdentityPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "123");
		
		Block tb = id.transform(sb);
		assertThat(tb.fields().length, is(1));
		assertThat(tb.fields()[0], is("C1"));
		assertThat(tb.value("C1"), is("123"));
	}
	


	@Test
	public void test_double_field() {
		IdentityPipe id = IdentityPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "123");
		sb.add("C2", "abc");
	}

	@Test
	public void test_two_fields() throws Exception {
		IdentityPipe id = IdentityPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "123");
		sb.add("C2", "456");

		
		Block tb = id.transform(sb);
		assertThat(tb.fields().length, is(2));
		assertThat(tb.fields()[0], is("C1"));

		assertThat(tb.fields()[1], is("C2"));
		assertThat(tb.value("C1"), is("123"));
		assertThat(tb.value("C2"), is("456"));
	}

	@Test
	public void test_many_field() {
		IdentityPipe id = IdentityPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "123");
		sb.add("C2", "abc");
		sb.add("C3", "456");
		sb.add("C4", "789");
		sb.add("C5", "qwe");
		sb.add("C6", "asd");
		sb.add("C7", "zxc");
		sb.add("C8", "mnb");
		
		Block tb = id.transform(sb);
		assertThat(tb.fields().length, is(8));
		assertThat(tb.fields()[0], is("C1"));
		assertThat(tb.fields()[1], is("C2"));
		assertThat(tb.fields()[2], is("C3"));
		assertThat(tb.fields()[3], is("C4"));
		assertThat(tb.fields()[4], is("C5"));
		assertThat(tb.fields()[5], is("C6"));
		assertThat(tb.fields()[6], is("C7"));
		assertThat(tb.fields()[7], is("C8"));
		assertThat(tb.value("C1"), is("123"));
		assertThat(tb.value("C2"), is("abc"));
		assertThat(tb.value("C3"), is("456"));
		assertThat(tb.value("C4"), is("789"));
		assertThat(tb.value("C5"), is("qwe"));
		assertThat(tb.value("C6"), is("asd"));
		assertThat(tb.value("C7"), is("zxc"));
		assertThat(tb.value("C8"), is("mnb"));
	}
	
	@Test
	public void test_special_field() throws Exception {
		IdentityPipe id = IdentityPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "123");
		sb.add("C2", "abc");
		sb.add("C3", "456");
		sb.add("C4", "789");
		
		Block tb = id.transform(sb);
		assertThat(tb.fields().length, is(4));
		assertThat(tb.fields()[2], is("C3"));
		assertThat(tb.value("C3"), is("456"));

		assertThat(tb.value("C1"), is("123"));
		assertThat(tb.fields()[1], is("C2"));
		assertThat(tb.value("C2"), is("abc"));
	}
	
	@Test
	public void test_three_fields() throws Exception {
		IdentityPipe id = IdentityPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "123");
		sb.add("C2", "456");
		sb.add("C3", "789");
		
		Block tb = id.transform(sb);
		assertThat(tb.fields().length, is(3));
		assertThat(tb.fields()[0], is("C1"));
		assertThat(tb.value("C1"), is("123"));
		assertThat(tb.fields()[1], is("C2"));
		assertThat(tb.value("C2"), is("456"));
		assertThat(tb.fields()[2], is("C3"));
		assertThat(tb.value("C3"), is("789"));

	}
}
