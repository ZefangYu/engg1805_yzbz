package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;

public class EvenFieldsPipeTest {
	
	@Test
	public void test_zero_columns() throws Exception {
	
		EvenFieldsPipe evp = EvenFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(0));
		
	}

	@Test
	public void test_three_columns() throws Exception {
	
		EvenFieldsPipe evp = EvenFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); //first col1umn
		sb.add("C2", "def"); //second column
		sb.add("C3", "GHI"); //third column
		
		
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(1));
		assertThat(tb.fields()[0], is("C2"));
	}
	
	@Test
	public void test_one_column() throws Exception {
	
		EvenFieldsPipe evp = EvenFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); //first col1umn
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(0));		
	}
	
	@Test
	public void test_two_columns() {
		EvenFieldsPipe evp = EvenFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); 
		sb.add("C2", "def");
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(1));
		assertThat(tb.fields()[0], is("C2"));
	}
	
	@Test
	public void test_four_columns() {
		EvenFieldsPipe evp = EvenFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); 
		sb.add("C2", "def");
		sb.add("C3", "GHI");
		sb.add("C4", "jkl");
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(2));
		assertThat(tb.fields()[0], is("C2"));
		assertThat(tb.fields()[1], is("C4"));
	}
	
	@Test
	public void test_five_columns() {
		EvenFieldsPipe evp = EvenFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); 
		sb.add("C2", "def");
		sb.add("C3", "GHI");
		sb.add("C4", "jkl");
		sb.add("C5", "MNO");
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(2));
		assertThat(tb.fields()[0], is("C2"));
		assertThat(tb.fields()[1], is("C4"));
	}
	
	@Test
	public void test_many_columns() throws Exception {
	
		EvenFieldsPipe evp = EvenFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); 
		sb.add("C2", "def"); 
		sb.add("C3", "GHI"); 
		sb.add("C4", "123"); 
		sb.add("C5", "456"); 
		sb.add("C6", "789"); 
		sb.add("C7", "qwe"); 
		sb.add("C8", "asd"); 
		
		
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(4));
		assertThat(tb.fields()[0], is("C2"));
		assertThat(tb.fields()[1], is("C4"));
		assertThat(tb.fields()[2], is("C6"));
		assertThat(tb.fields()[3], is("C8"));
	}
	

	@Test
	public void test_special_columns() throws Exception {
	
		EvenFieldsPipe evp = EvenFieldsPipe.create();
		SimpleBlock sb = new SimpleBlock();
		sb.add("C1", "ABC"); 
		sb.add("C2", "def"); 
		sb.add("C3", "GHI"); 
		sb.add("C4", "123"); 
		sb.add("C5", "456"); 
		sb.add("C6", "789"); 
		sb.add("C7", "qwe"); 
		sb.add("C8", "asd"); 
		sb.add("C9", "wer"); 
		sb.add("C10", "dfg"); 
		
		
		Block tb = evp.transform(sb);
		assertThat(tb.fields().length, is(5));
		assertThat(tb.fields()[1], is("C4"));
	}
}

